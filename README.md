A project for GitLab test and development.

- [Check for gitlab-org/gitlab-ce#17276](gitlab-org-gitlab-ce-17276/README.md)
- "Cherry-pick"ing the merged MR on the forked projects into the original projects